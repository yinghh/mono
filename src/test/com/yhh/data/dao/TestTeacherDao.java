package com.yhh.data.dao;

import com.yhh.data.entity.Teacher;
import com.yhh.data.service.TeacherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class TestTeacherDao {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private TeacherService teacherDao;

    @Test
    public void selectBook() {
        Teacher teacher = teacherDao.selectTeacher(1);
        if (teacher != null) {
            logger.info(teacher.toString());
        }
    }


    @Test
    public void deleteBook() {
        Teacher teacher = teacherDao.selectTeacher(1);
        if (teacher == null) {
            teacher = new Teacher();
            teacher.setName("数学");
            teacher.setId(1);
            Integer id = teacherDao.saveTeacher(teacher);
            logger.info(id.toString());
        }
        teacherDao.deleteTeacher(teacher);
    }

    @Test
    public void saveBook() {
        Teacher teacher = teacherDao.selectTeacher(2);
        if (teacher != null) {
            teacherDao.deleteTeacher(teacher);
        }
        teacher = new Teacher();
        teacher.setName("数学");
        teacher.setId(2);
        Integer id = teacherDao.saveTeacher(teacher);
        logger.info(id.toString());
    }


    public void updateBook() {
        Teacher teacher = teacherDao.selectTeacher(2);
        if (teacher != null) {
            teacherDao.deleteTeacher(teacher);
        }
        teacher = new Teacher();
        teacher.setName("数学");
        teacher.setId(2);
        Integer id = teacherDao.saveTeacher(teacher);
        logger.info(id.toString());
        teacher.setName("语文");
        teacherDao.updateTeacher(teacher);
    }

}
