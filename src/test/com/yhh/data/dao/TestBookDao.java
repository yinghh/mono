package com.yhh.data.dao;

import com.yhh.data.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class TestBookDao {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private BookDao bookDao;

    @Test
    public void selectBook() {
        Book book = bookDao.selectBook(1);
        if (book != null) {
            logger.info(book.toString());
        }
    }

    @Test
    public void deleteBook() {
        Book book = bookDao.selectBook(1);
        if (book == null) {
            book = new Book();
            book.setName("数学");
            book.setId(1);
            book.setCreateTime(new Date());
            book.setUpdateTime(new Date());
            Integer id = bookDao.saveBook(book);
            logger.info(id.toString());
        }
        bookDao.deleteBook(book);
    }


    @Test
    public void saveBook() {
        Book book = bookDao.selectBook(2);
        if (book != null) {
            bookDao.deleteBook(book);
        }
        book = new Book();
        book.setName("数学");
        book.setId(2);
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        Integer id = bookDao.saveBook(book);
        logger.info(id.toString());
    }


    public void updateBook() {
        Book book = bookDao.selectBook(2);
        if (book != null) {
            bookDao.deleteBook(book);
        }
        book = new Book();
        book.setName("数学");
        book.setId(2);
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        Integer id = bookDao.saveBook(book);
        logger.info(id.toString());
        book.setName("语文");
        bookDao.updateBook(book);
    }

}
