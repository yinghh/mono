package com.yhh.data.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class User implements Serializable {
    private static final long serialVersionUID = 5010015939239210945L;
    private String id;
    private String username;
    private String password;
    private Date createTime;
    private Date expireTime;
}