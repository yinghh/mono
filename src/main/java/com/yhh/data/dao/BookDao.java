package com.yhh.data.dao;

import com.yhh.data.entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public interface BookDao {


    /**
     * 根据ID查询数据
     */
    Book selectBook(Integer id);

    /**
     * 保存数据
     */
    Integer saveBook(Book book);

    /**
     * 更新数据
     */
    void updateBook(Book book);

    /**
     * 删除数据
     */
    void deleteBook(Book book);
}
