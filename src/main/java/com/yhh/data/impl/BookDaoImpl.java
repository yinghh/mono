package com.yhh.data.impl;


import com.yhh.data.dao.BookDao;
import com.yhh.data.entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookDaoImpl implements BookDao {
    @Autowired
    private SessionFactory sessionfactory;

    /**
     * 获取与当前线程绑定的session
     */
    private Session getSession() {
        return sessionfactory.getCurrentSession();
    }

    /**
     * 根据ID查询数据
     */
    @Override
    public Book selectBook(Integer id) {
        Session session = this.getSession();
        Transaction tx = session.beginTransaction();
        Book book = (Book) session.get(Book.class, id);
        tx.commit();
        return book;
    }

    /**
     * 保存数据
     */
    @Override
    public Integer saveBook(Book book) {
        Session session = this.getSession();
        Integer id = (Integer) session.save(book);
        return id;
    }

    /**
     * 更新数据
     */
    @Override
    public void updateBook(Book book) {
        Session session = this.getSession();
        Transaction tx = session.beginTransaction();
        session.update(book);
        tx.commit();
    }

    @Override
    public void deleteBook(Book book) {
        Session session = this.getSession();
        Transaction tx = session.beginTransaction();
        session.delete(book);
        tx.commit();
    }
}
