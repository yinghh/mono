package com.yhh.data.impl;


import com.yhh.data.dao.TeacherDao;
import com.yhh.data.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherDaoImpl implements TeacherDao {
    @Autowired
    private SessionFactory sessionfactory;

    /**
     * 获取与当前线程绑定的session
     */
    private Session getSession() {
        return sessionfactory.getCurrentSession();
    }

    /**
     * 根据ID查询数据
     */
    @Override
    public Teacher selectTeacher(Integer id) {
        Session session = this.getSession();
        Teacher teacher = (Teacher) session.get(Teacher.class, id);
        return teacher;
    }

    /**
     * 保存数据
     */
    @Override
    public Integer saveTeacher(Teacher teacher) {
        Session session = this.getSession();
        Integer id = (Integer) session.save(teacher);
        return id;
    }

    /**
     * 更新数据
     */
    @Override
    public void updateTeacher(Teacher teacher) {
        Session session = this.getSession();
        session.update(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        Session session = this.getSession();
        session.delete(teacher);
    }


}
