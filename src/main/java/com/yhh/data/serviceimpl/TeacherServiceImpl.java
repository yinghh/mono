package com.yhh.data.serviceimpl;

import com.yhh.data.dao.TeacherDao;
import com.yhh.data.entity.Teacher;
import com.yhh.data.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDao teacherDao;

    @Override
    public Teacher selectTeacher(Integer id) {
        return teacherDao.selectTeacher(id);
    }

    @Override
    public Integer saveTeacher(Teacher teacher) {
        return teacherDao.saveTeacher(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherDao.updateTeacher(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        teacherDao.deleteTeacher(teacher);
    }
}
